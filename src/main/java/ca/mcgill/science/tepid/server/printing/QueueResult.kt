package ca.mcgill.science.tepid.server.printing

data class QueueResult(val destination: String, val eta: Long)
